module fp_cmp_leq_p0 #(
  parameter WE = 5,
  parameter WF = 10
)(
  input aclk,
  input aresetn,
  input [WE+WF+2:0] A_TDATA,
  input             A_TVALID,
  input [WE+WF+2:0] B_TDATA,
  input             B_TVALID,
  output            R_TDATA,
  output            R_TVALID
);

// common
wire [1:0]    excpt_a;
wire          sign_a;
wire [WE-1:0] expo_a;
wire [WF-1:0] manti_a;
wire [1:0]    excpt_b;
wire          sign_b;
wire [WE-1:0] expo_b;
wire [WF-1:0] manti_b;
wire both_zero;
wire both_norm;
reg  both_zero_d1;
reg  both_norm_d1;
wire out_vld;
reg  out_vld_d1;

// equal
wire sign_eq;
wire value_eq;
wire both_inf;
reg  sign_eq_d1;
reg  value_eq_d1;
reg  both_inf_d1;
wire r_eq;

// grtr/less common
wire r_nan;
wire expo_eq;
wire isPos;
wire isNeg;
wire excpt_grtr;
wire expo_grtr;
wire manti_grtr;
wire sign_grtr;
wire excpt_less;
wire expo_less;
wire manti_less;
wire sign_less;
reg  r_nan_d1;
reg  expo_eq_d1;
reg  isPos_d1;
reg  isNeg_d1;
reg  excpt_grtr_d1;
reg  expo_grtr_d1;
reg  manti_grtr_d1;
reg  sign_grtr_d1;
reg  excpt_less_d1;
reg  expo_less_d1;
reg  manti_less_d1;
reg  sign_less_d1;
wire r_grtr;
wire r_less;

// common
assign {excpt_a, sign_a, expo_a, manti_a} = A_TDATA;
assign {excpt_b, sign_b, expo_b, manti_b} = B_TDATA;
assign both_zero = (excpt_a == 2'b00 && excpt_b == 2'b00);
assign both_norm = (excpt_a == 2'b01 && excpt_b == 2'b01);

always @(*) begin
  both_zero_d1 = both_zero;
  both_norm_d1 = both_norm;
end

// equal
assign sign_eq = (sign_a == sign_b);
assign value_eq =             ({sign_a, expo_a, manti_a} == {sign_b, expo_b, manti_b});
assign both_inf = (excpt_a == 2'b10 && excpt_b == 2'b10);

always @(*) begin
  sign_eq_d1 = sign_eq;
  value_eq_d1 = value_eq;
  both_inf_d1 = both_inf;
end

assign r_eq = both_zero_d1 || (both_inf_d1 && sign_eq_d1) || (both_norm_d1 && value_eq_d1);

// grtr/less common
assign r_nan = (excpt_a == 2'b11 || excpt_b == 2'b11);
assign expo_eq = (expo_a == expo_b);
assign isPos = (sign_a == 1'b0 && sign_b == 1'b0);
assign isNeg = (sign_a == 1'b1 && sign_b == 1'b1);
assign excpt_grtr = (excpt_a > excpt_b);
assign expo_grtr = (expo_a > expo_b);
assign manti_grtr = (manti_a > manti_b);
assign excpt_less = (excpt_a < excpt_b);
assign expo_less = (expo_a < expo_b);
assign manti_less = (manti_a < manti_b);
assign sign_grtr = (sign_a == 1'b0 && sign_b == 1'b1);
assign sign_less = (sign_a == 1'b1 && sign_b == 1'b0);

always @(*) begin
  r_nan_d1 = r_nan;
  expo_eq_d1 = expo_eq;
  isPos_d1 = isPos;
  isNeg_d1 = isNeg;
  excpt_grtr_d1 = excpt_grtr;
  expo_grtr_d1 = expo_grtr;
  manti_grtr_d1 = manti_grtr;
  excpt_less_d1 = excpt_less;
  expo_less_d1 = expo_less;
  manti_less_d1 = manti_less;
  sign_grtr_d1 = sign_grtr;
  sign_less_d1 = sign_less;
end

// less
assign r_less = (!r_nan_d1 && !both_zero_d1 && (sign_less_d1 || 
  (isPos_d1 && (excpt_less_d1 || 
  (both_norm_d1 && (expo_less_d1 || (expo_eq_d1 && manti_less_d1))))) || 
  (isNeg_d1 && (excpt_grtr_d1 ||
  (both_norm_d1 && (expo_grtr_d1 || (expo_eq_d1 && manti_grtr_d1))))) 
));

// less
assign out_vld = A_TVALID & B_TVALID;
assign R_TDATA = r_less | r_eq;

always @(*) begin
  out_vld_d1 = out_vld;
end

assign R_TVALID = out_vld_d1;
endmodule
// fp_cmp_leq
