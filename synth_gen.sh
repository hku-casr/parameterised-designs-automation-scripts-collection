#!/bin/bash

#
# Name: synth_gen.sh
#
# Description: This script puts a floating-point ALUs into a placeholder of a
#  template project and runs Xilinx Vivado Synthesis to obtain FPGA resource
#  utilisation report of individual ALU-under-test.
#
#  This script is part of a toolchain for automating the generation of results
#  for a paper to be submitted to FCCM 2017 by CASR group member.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

source /vol/tools/Xilinx/Vivado/2015.4/settings64.sh;
export XIL_CSE_PLUGIN_DIR=$HOME/Xilinx_CSE_plugin/;
export XILINXD_LICENSE_FILE=2200@bluemountain.eee.hku.hk;

nthd=8;

if [ -z "$expb" ] || [ -z "$matb" ]; then
    exit 1;
fi;

# expb=5;
# matb=10;

tmpl="/home/chdhung/FCCM2017/tmpl_nncore_generic_synth_$expb$matb";
proj="/home/chdhung/FCCM2017/nncore_generic_synth_$expb$matb";
dstf="sources_1/new/fp_core.v";

vivq="\[Common 17-206\] Exiting Vivado";

while read file; do
    elvl=$(echo $file | rev | cut -d "/" -f2 | rev | cut -d "-" -f2);
    func=$(echo $file | rev | cut -d "/" -f1 | rev | cut -d "_" -f2);
    sect=$(echo $file | rev | cut -d "/" -f1 | rev | cut -d "." -f1 | cut -d "_" -f5);
    odda=$(echo $file | rev | cut -d "/" -f1 | rev | cut -d "." -f1 | cut -d "_" -f6);

    if [ -z "$odda" ]; then
        subv="$func-e$elvl-$sect";
    else
        subv="$func-e$elvl-$sect-odd";
    fi;

    if [ "${func:0:4}" == "relu" ]; then
        sprj="nncore_generic_synth_$expb${matb}_relu.srcs";
        ext="_relu";

        cp -r $tmpl$ext $proj\_$subv;

        subs=$(cat $file | grep "module\ fp\_relu.*\ \#(");
        cat $file | sed -e "s/module\ fp\_relu.*\ \#(/\/\/ $subs\nmodule\ fp\_core\ \#(/g" | tee $proj\_$subv/$sprj/$dstf > /dev/null;
    else
        sprj="nncore_generic_synth_$expb$matb.srcs";

        cp -r $tmpl$ext $proj\_$subv;

        subs=$(cat $file | grep "module\ fp\_.*\_$expb\_$matb\_d.*\ \#(");
        cat $file | sed -e "s/module\ fp\_.*\_$expb\_$matb\_d.*\ \#(/\/\/ $subs\nmodule\ fp\_core\ \#(/g" | tee $proj\_$subv/$sprj/$dstf > /dev/null;
    fi;

    tclf=`mktemp`;

    cat > $tclf <<!
open_project $proj\_$subv/nncore_generic_synth_$expb$matb$ext.xpr
launch_runs synth_1 -jobs $nthd
!

    /vol/tools/Xilinx/Vivado/2015.4/bin/vivado -mode batch -source $tclf -nolog -nojournal > /dev/null;

    while [ `tail -n 10 $proj\_$subv/nncore_generic_synth_$expb$matb$ext.runs/synth_1/runme.log | grep "$vivq" | wc -l 2> /dev/null` -eq 0 ]; do sleep 1; done;

    echo "Synth Done!";

    rm -rf $tclf;
done < $1;
