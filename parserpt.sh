#!/bin/bash

#
# Name: parserpt.sh
#
# Description: This script parses Xilinx Vivado synthesis utilisation report for
#  Slice LUT, Slice Register, RAM Block (RAMB18) and DSP usage statistics for
#  synthesised design.
#
# Author: Maolin WANG <mlwang@eee.hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

grep "Slice LUT" $1 | grep \| | cut -d "|" -f3
grep "Slice Register" $1 | grep \| | cut -d "|" -f3
grep "RAMB18 " $1 | grep \| | cut -d "|" -f3
grep "DSPs" $1 | grep \| | cut -d "|" -f3
