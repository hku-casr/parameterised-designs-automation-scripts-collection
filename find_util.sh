#!/bin/bash

#
# Name: find_util.sh
#
# Description: This script queries the FPGA resource utilisation of a Xilinx
#  Vivado synthesised project.
#
#  This script is part of a toolchain for automating the generation of results
#  for a paper to be submitted to FCCM 2017 by CASR group member.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

if [ -z "$expb" ] || [ -z "$matb" ]; then
    exit 1;
fi;

proj="/home/chdhung/FCCM2017/nncore_generic_synth_$expb$matb";
rptf="nncore_generic_synth_$expb$matb*.runs/synth_1/fp_core_utilization_synth.rpt";

for path in `find $(pwd) -path "$proj*/$rptf"`; do
    elvl=$(echo $path | rev | cut -d "/" -f4 | rev | cut -d "-" -f2);
    func=$(echo $path | rev | cut -d "/" -f4 | cut -d "_" -f1 | rev | cut -d "-" -f1);
    sect=$(echo $path | rev | cut -d "/" -f4 | cut -d "_" -f1 | rev | cut -d "-" -f3);
    odda=$(echo $path | rev | cut -d "/" -f4 | cut -d "_" -f1 | rev | cut -d "-" -f4);

    outf=`mktemp`;

    echo -ne "$func:$elvl:$sect:$odda:LUT|REG|RAMB18|DSPs";

    ./parserpt.sh $path > $outf;

    for i in `seq 1 4`; do
        r=$(cat $outf | head -n $i | tail -n 1 | sed -e "s/[[:space:]]*\([[:digit:]]*\)[[:space:]]*/\1/g");
        echo -ne ":$r";
    done;

    echo "";

    rm -rf $outf;
done;
