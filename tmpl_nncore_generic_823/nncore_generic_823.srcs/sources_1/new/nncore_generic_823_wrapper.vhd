--
-- NNCORE_GENERIC_823_WRAPPER.VHD
--
-- Short Description:
--
-- Long Description:
--
-- Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
--
-- Author: C.-H. Dominic HUNG <chdhung@hku.hk>
--  Computer Architecture and System Research,
--  Department of Electrical and Electronic Engineering,
--  The University of Hong Kong
--

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY NNCORE_GENERIC_823_WRAPPER IS
    GENERIC(
        WE       : INTEGER := 8;
        WF       : INTEGER := 23
    );
    PORT(
        CLK200       : IN    STD_LOGIC
    );
END ENTITY NNCORE_GENERIC_823_WRAPPER;

ARCHITECTURE BEHAVIOURAL OF NNCORE_GENERIC_823_WRAPPER IS
    COMPONENT FP_CORE IS
        GENERIC(
            WE       : INTEGER := 8;
            WF       : INTEGER := 23
        );
        PORT(
            ACLK     : IN    STD_LOGIC;
            ARESETN  : IN    STD_LOGIC;
            A_TDATA  : IN    STD_LOGIC_VECTOR(WE + WF + 2 DOWNTO 0);
            A_TVALID : IN    STD_LOGIC;
            R_TDATA  : OUT   STD_LOGIC_VECTOR(WE + WF + 2 DOWNTO 0);
            R_TVALID : OUT   STD_LOGIC
        );
    END COMPONENT;

    COMPONENT RESET_PANEL IS
        PORT(
            CLK           : IN    STD_LOGIC;
            PROBE_OUT0    : OUT   STD_LOGIC;
            PROBE_IN0     : IN    STD_LOGIC;
            PROBE_IN1     : IN    STD_LOGIC;
            PROBE_IN2     : IN    STD_LOGIC;
            PROBE_IN3     : IN    STD_LOGIC;
            PROBE_IN4     : IN    STD_LOGIC
        );
    END COMPONENT;

    COMPONENT VIO_0 IS
        PORT(
           CLK            : IN    STD_LOGIC;
           PROBE_OUT0     : OUT   STD_LOGIC_VECTOR(33 DOWNTO 0);
           PROBE_OUT1     : OUT   STD_LOGIC;
           PROBE_IN0      : IN    STD_LOGIC_VECTOR(33 DOWNTO 0);
           PROBE_IN1      : IN    STD_LOGIC
        );
    END COMPONENT;

    SIGNAL RESET_LINE    : STD_LOGIC;

    SIGNAL LED_IDLE     : STD_LOGIC;
    SIGNAL LED_READY    : STD_LOGIC;
    SIGNAL LED_DONE     : STD_LOGIC;
    SIGNAL LED_READ     : STD_LOGIC;

    SIGNAL FEED_TDATA    : STD_LOGIC_VECTOR(WE + WF + 2 DOWNTO 0);
    SIGNAL FEED_TVALID   : STD_LOGIC;

    SIGNAL RET_TDATA     : STD_LOGIC_VECTOR(WE + WF + 2 DOWNTO 0);
    SIGNAL RET_TVALID    : STD_LOGIC;
BEGIN

    RST: RESET_PANEL PORT MAP(
        CLK200,
        RESET_LINE,
        LED_IDLE,
        LED_READY,
        LED_DONE,
        LED_READ,
        FEED_TVALID
    );

    UUT: FP_CORE GENERIC MAP(
        WE => WE,
        WF => WF
    ) PORT MAP(
        CLK200,
        RESET_LINE, -- '1',
        FEED_TDATA,
        FEED_TVALID,
        RET_TDATA,
        RET_TVALID
    );

    KEEPER: VIO_0 PORT MAP(
        CLK200,
        FEED_TDATA,
        FEED_TVALID,
        RET_TDATA,
        RET_TVALID
    );
END ARCHITECTURE;