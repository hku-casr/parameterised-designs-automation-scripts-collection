#!/bin/bash

#
# Name: find_wns.sh
#
# Description: This script queries the worst negative slack of a Xilinx Vivado
#  project implementation.
#
#  This script is part of a toolchain for automating the generation of results
#  for a paper to be submitted to FCCM 2017 by CASR group member.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

source /vol/tools/Xilinx/Vivado/2015.4/settings64.sh;
export XIL_CSE_PLUGIN_DIR=$HOME/Xilinx_CSE_plugin/;
export XILINXD_LICENSE_FILE=2200@bluemountain.eee.hku.hk;

if [ -z "$expb" ] || [ -z "$matb" ]; then
    exit 1;
fi;

proj="/home/chdhung/FCCM2017/nncore_generic_$expb$matb";
prjf="nncore_generic_$expb$matb*.xpr";

for path in `find $(pwd) -path "$proj*/$prjf"`; do
    elvl=$(echo $path | rev | cut -d "/" -f2 | rev | cut -d "-" -f2);
    func=$(echo $path | rev | cut -d "/" -f2 | cut -d "_" -f1 | rev | cut -d "-" -f1);
    sect=$(echo $path | rev | cut -d "/" -f2 | cut -d "_" -f1 | rev | cut -d "-" -f3);
    odda=$(echo $path | rev | cut -d "/" -f2 | cut -d "_" -f1 | rev | cut -d "-" -f4);

    tclf=`mktemp`;

    cat > $tclf <<!
open_project $path
set wns [get_property STATS.WNS [get_runs impl_1]]
set dsc "WNS"
puts "\$dsc:\$wns"
!

    outf=`mktemp`;

    /vol/tools/Xilinx/Vivado/2015.4/bin/vivado -mode batch -source $tclf -nolog -nojournal > $outf;

    echo -ne "$func:$elvl:$sect:$odda:";
    cat $outf | grep "WNS:";

    rm -rf $outf;

    rm -rf $tclf;
done;
